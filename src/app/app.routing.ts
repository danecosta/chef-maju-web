import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
    {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'listar-receitas/:uf/:cidade',
        loadChildren: () => import('./pages/listar-receitas/listar-receitas.module').then(m => m.ListarReceitasModule)
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    }
];