import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'chef-maju-web';

  constructor(private router: Router) { }

  IrParaHome() {
    this.router.navigate(['home/']);
  }
}
