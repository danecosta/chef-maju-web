import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';

@Injectable()
export class ReceitaService extends BaseService<any> {

  constructor(http: HttpClient) {
    super(http, 'receita');
  }
}
