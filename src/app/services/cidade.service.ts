import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CidadeService extends BaseService<any> {

  constructor(http: HttpClient) {
    super(http, 'cidade');
  }
}
