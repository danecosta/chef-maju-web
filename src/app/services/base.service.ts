import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { catchError } from 'rxjs/operators';

export abstract class BaseService<T> {

  protected _http: HttpClient;
  protected baseUrl: string = environment.apiUrl;

  public constructor(private http: HttpClient, private resource: string) {
    this._http = http;
    this.baseUrl += resource && resource.length > 1 ? `/${resource}` : '';
  }

  public get(action: string = ''): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}/${action}`).pipe(catchError(this.handleError));
  }

  public getById(action: string = '', id: string): Observable<any> {
    return this._http.get<any>(`${this.baseUrl}/${action}${id}`).pipe(catchError(this.handleError));
  }

  public getAll(action: string = ''): Observable<any[]> {
    return this._http.get<any[]>(`${this.baseUrl}/${action}`).pipe(catchError(this.handleError));
  }

  protected handleError(error: Response | any) {
    return Observable.throw(error);
  }
}
