import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { ReceitaService } from 'src/app/services/receita.service';
import { ListarReceitasComponent } from './listar-receitas.component';
import { ListarReceitasRouting } from './listar-receitas.routing';

@NgModule({
    declarations: [
        ListarReceitasComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpClientModule,
        ListarReceitasRouting,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatGridListModule,
        MatCardModule,
        MatListModule,
        MatChipsModule,
        MatIconModule,
        MatProgressBarModule
    ],
    providers: [
        ReceitaService
    ]
})
export class ListarReceitasModule { }
