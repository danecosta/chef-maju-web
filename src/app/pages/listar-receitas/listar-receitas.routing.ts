import { Routes, RouterModule } from '@angular/router';

import { ModuleWithProviders } from '@angular/core';
import { ListarReceitasComponent } from '../listar-receitas/listar-receitas.component';

export const listarReceitasRoutes: Routes = [
    {
        path: '',
        component: ListarReceitasComponent,
        data: {
            pageTitle: 'Listar Receitas'
        }
    },
    {
        path: 'listar-receitas/:uf/:cidade',
        component: ListarReceitasComponent,
        data: {
            pageTitle: 'Listar Receitas por Cidade'
        }
    }
];

export const ListarReceitasRouting: ModuleWithProviders = RouterModule.forChild(listarReceitasRoutes);

