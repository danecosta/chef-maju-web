import { Component, OnInit } from '@angular/core';
import { ReceitaService } from 'src/app/services/receita.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-listar-receitas',
  templateUrl: './listar-receitas.component.html',
  styleUrls: ['./listar-receitas.component.css']
})
export class ListarReceitasComponent implements OnInit {

  cidadeSelecionada: string;
  estadoSelecionado: string;
  model: any;
  cidadeEncontrada = false;
  exibeMsgCidadeNaoEncontrada = false;

  constructor(private receitaService: ReceitaService,
    private route: ActivatedRoute,
    private router: Router) {
    this.estadoSelecionado = this.route.snapshot.params.uf;
    this.cidadeSelecionada = this.route.snapshot.params.cidade;
  }

  ngOnInit() {
    if (this.cidadeSelecionada) {
      this.buscarReceitasPorCidade();
    }
  }

  buscarReceitasPorCidade() {
    this.receitaService.get(`obter-por-cidade?cidade=${this.cidadeSelecionada}`).subscribe(
      data => {
        this.model = data;

        if (this.model.clima && this.model.receitas.length > 0) {
          this.cidadeEncontrada = true;
        } else {
          this.exibeMsgCidadeNaoEncontrada = true;
        }
      }
    );
  }

  novaBusca() {
    this.router.navigate(['home/']);
  }

  abrirNovaAba(url) {
    window.open(url, '_blank');
  }

}
