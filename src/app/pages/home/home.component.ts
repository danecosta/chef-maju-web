import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { EstadoService } from 'src/app/services/estado.service';
import { CidadeService } from 'src/app/services/cidade.service';
import { Estado } from 'src/app/models/estado.model';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  estadoSelecionado = new FormControl();
  estados: Estado[] = [];
  estadosFiltrados: Observable<Estado[]>;

  cidadeSelecionada = new FormControl();
  cidades: string[] = [];
  cidadesFiltradas: Observable<string[]>;

  constructor(public estadoService: EstadoService,
    public cidadeService: CidadeService,
    public router: Router) {
    this.estadoSelecionado = new FormControl('', [Validators.required,]);
    this.cidadeSelecionada = new FormControl('', [Validators.required,]);
  }

  ngOnInit() {
    this.buscarEstados();

    this.estadosFiltrados = this.estadoSelecionado.valueChanges
      .pipe(startWith(''), map(value => this.filtrarEstado(value)));

    this.cidadesFiltradas = this.cidadeSelecionada.valueChanges
      .pipe(startWith(''), map(value => this.filtrarCidade(value)));

  }

  buscarEstados() {
    this.estadoService.getAll().subscribe(
      data => {
        this.estados = data;
      },
      error => {
        console.log(error);
      },
    );
  }

  buscarCidade(event) {
    this.cidadeService.get(`obter-por-uf?uf=${event.option.value}`).subscribe(
      data => {
        this.cidades = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  private filtrarEstado(value: string): Estado[] {
    if (this.estados.length > 0) {
      const filtro = value.toLowerCase();
      return this.estados.filter(selecionada => selecionada.nome.toLowerCase().includes(filtro));
    }
  }


  private filtrarCidade(value: string): string[] {
    if (this.cidades.length > 0) {
      const filtro = value.toLowerCase();

      return this.cidades.filter(estado => estado.toLowerCase().includes(filtro));
    }
  }

  buscarReceita() {
    if (this.estadoSelecionado.valid && this.cidadeSelecionada.valid) {
      this.router.navigate(['listar-receitas/', this.estadoSelecionado.value, this.cidadeSelecionada.value]);
    }
  }

}
